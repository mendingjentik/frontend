import axios from "axios";
import useSWR from "swr";

export default function useFetch({ url, query, disable, options }) {
  if (typeof query === "object")
    query = Object.keys(query)
      .map(
        (key) =>
          query[key] &&
          `${key}=${
            typeof query[key] === "object"
              ? JSON.stringify(query[key])
              : query[key]
          }`
      )
      .filter((a) => a)
      .join("&");

  const fetcher = (url) => {
    return axios
      .get(`https://mendingjentikbe.herokuapp.com${url}`)
      .then((res) => {
        return res;
      })
      .catch((error) => {
        return error.response.data;
      });
  };

  const { data, error, isValidating } = useSWR(
    disable ? null : `${url}?${query ? query : ""}`,
    fetcher,
    options
  );

  return { data: data?.data, rawData: data, query, error, isValidating };
}
