import { useEffect, useState } from "react";
import { AnimatePresence, motion } from "framer-motion";
import { useRouter } from "next/dist/client/router";
import Popup from "@components/pages/search/Popup";
import useFetch from "hooks/useFetch";
import { useDebouncedValue } from "rooks";

export default function Search() {
  const router = useRouter();

  const [title, setTitle] = useState(router.query.title);
  const [filter, setFilter] = useState(router.query);
  const [debouncedTitle] = useDebouncedValue(title, 750);
  const [debouncedYear] = useDebouncedValue(filter.year, 750);
  const [debouncedActor] = useDebouncedValue(filter.actor, 750);
  const [debouncedDirector] = useDebouncedValue(filter.director, 750);
  const [debouncedGenre] = useDebouncedValue(filter.genre, 750);

  const { rawData, data, error } = useFetch({
    url: "/",
    query: { ...router.query },
  });

  const [selected, setSelected] = useState({});

  const handleSelect = (content) => {
    setSelected({});
    setTimeout(() => {
      setSelected(content);
    }, 300);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setSelected("");
    router.push(
      { pathname: "/search", query: { ...router.query, ...filter, title } },
      undefined,
      { shallow: true }
    );
  };

  useEffect(() => {
    handleSubmit({ preventDefault: () => {} });
  }, [
    debouncedTitle,
    debouncedActor,
    debouncedDirector,
    debouncedGenre,
    debouncedYear,
  ]);

  return (
    <div className="flex justify-center w-full relative">
      <div className="max-w-screen-xl w-full mx-4 sm:mx-12">
        <div className="sticky top-0 bg-white">
          <div className="pt-4 mt-8 bg-white">
            <div className="flex items-center">
              <motion.i
                animate={{ opacity: 1, transition: { delay: 0.3 } }}
                initial={{ opacity: 0 }}
                className="text-gray-600 fas fa-chevron-left mr-8 text-xl relative top-0.5 cursor-pointer"
                onClick={() => router.push("/")}
              />
              <motion.h1
                className="text-lg sm:text-2xl font-weight-bold tracking-widest text-gray-600 bg-white"
                layoutId="header"
                layout="position"
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
              >
                MENDING JENTIK /
              </motion.h1>
              <form onSubmit={handleSubmit}>
                <motion.input
                  initial={{ opacity: 0, x: -100 }}
                  animate={{ opacity: 1, x: 0, transition: { delay: 0.4 } }}
                  className="text-lg sm:text-2xl font-weight-bold tracking-widest text-gray-600 ml-4 outline-none border-b"
                  value={title}
                  onChange={(e) => setTitle(e.target.value)}
                  placeholder="search..."
                />
              </form>
            </div>
          </div>
          <motion.div
            className="mx-4 sm:mx-11 mt-4"
            animate={{ opacity: 1, transition: { delay: 0.2 } }}
            initial={{ opacity: 0 }}
            exit={{ opacity: 0 }}
          >
            <form onSubmit={handleSubmit} className="flex justify-between">
              {["Year", "Actor", "Director", "Genre"].map((searchable) => {
                return (
                  <div className="mr-4">
                    <span className="font-semibold">{searchable}</span>:
                    <input
                      className="outline-none border-b mx-2"
                      name={searchable}
                      value={filter[searchable.toLowerCase()]}
                      onChange={(e) =>
                        setFilter({
                          ...filter,
                          [searchable.toLowerCase()]: e.target.value,
                        })
                      }
                    />
                  </div>
                );
              })}
            </form>
          </motion.div>
          <motion.hr
            initial={{ width: 0 }}
            animate={{ width: "auto", transition: { duration: 1.5 } }}
            exit={{ width: 0, transition: { duration: 0.2 } }}
            className="mx-4 sm:mx-11 mt-4"
          />
        </div>

        <div className="flex">
          <motion.div
            animate={{ opacity: 1, transition: { delay: 0.2 } }}
            initial={{ opacity: 0 }}
            exit={{ opacity: 0 }}
            className="mx-4 sm:mx-11 my-8 w-1/2"
          >
            {data ? (
              data?.map((content) => (
                <motion.div
                  key={content.title}
                  animate={{ opacity: 1 }}
                  initial={{ opacity: 0 }}
                  exit={{ opacity: 0 }}
                >
                  <h4
                    className="text-2xl font-semibold tracking-wide cursor-pointer mt-4"
                    onClick={() => handleSelect(content)}
                  >
                    {content.title}
                  </h4>
                  <p
                    className="text-base max-w-xl overflow-hidden text-gray-500"
                    style={{
                      WebkitLineClamp: 2,
                      WebkitBoxOrient: "vertical",
                      display: "-webkit-box",
                    }}
                  >
                    {content.description == "nan" ? "" : content.description}
                  </p>
                </motion.div>
              ))
            ) : (
              <motion.div
                animate={{ opacity: 1 }}
                initial={{ opacity: 0 }}
                exit={{ opacity: 0 }}
              >
                {rawData ? (
                  <div className="tracking-widest text-3xl h-full text-red-500">
                    {rawData.response}
                  </div>
                ) : (
                  <div className="tracking-widest text-3xl h-full">
                    LOADING...
                  </div>
                )}
              </motion.div>
            )}
            {Array.isArray(data) && !data?.length && (
              <div className="tracking-widest text-3xl h-full">EMPTY DATA</div>
            )}
          </motion.div>
          <div>
            <AnimatePresence>
              {Object.keys(selected).length ? <Popup {...selected} /> : ""}
            </AnimatePresence>
          </div>
        </div>
      </div>
    </div>
  );
}

export async function getServerSideProps() {
  return { props: {} };
}
