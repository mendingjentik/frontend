import { motion } from "framer-motion";
import { useRouter } from "next/dist/client/router";
import { useState, Fragment } from "react";
import BgAnimation from "@components/pages/home/BackgroundAnimations";

export default function Home() {
  const router = useRouter();

  const [val, setVal] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    router.push({ pathname: "/search", query: { title: val } });
  };

  return (
    <Fragment>
      <div
        className="overflow-hidden absolute w-screen h-screen"
        style={{ zIndex: -1 }}
      >
        {Array(50)
          .fill(0)
          .map((_, index) => (
            <BgAnimation key={index} />
          ))}
      </div>

      <div className="flex justify-center items-center h-screen text-center">
        <div className="mb-16 mx-4">
          <div className="text-5xl font-weight-bold tracking-widest">
            <motion.h1
              animate={{ opacity: 1 }}
              initial={{ opacity: 0 }}
              layoutId="header"
              layout="position"
              className="text-gray-600"
            >
              MENDING JENTIK
            </motion.h1>
            <motion.h1
              animate={{ opacity: 1, transition: { delay: 0.2 } }}
              initial={{ opacity: 0 }}
              exit={{ opacity: 0, transition: { duration: 0.1 } }}
              className="text-4xl mt-8 text-gray-300"
            >
              Search Engine
            </motion.h1>
          </div>
          <div className="mt-12">
            <motion.div
              animate={{
                opacity: 1,
                transition: { delay: 0.4 },
              }}
              initial={{ opacity: 0 }}
              exit={{ opacity: 0, transition: { duration: 0.1 } }}
              className=""
            >
              <form
                className="flex items-center shadow-md rounded-xl"
                onSubmit={handleSubmit}
              >
                <input
                  className="w-full h-12 outline-none border-2 border-gray-400 px-2 rounded-l-lg tracking-widest font-semibold"
                  placeholder="Search..."
                  value={val}
                  onChange={(e) => setVal(e.target.value)}
                />
                <button
                  type="submit"
                  className="cursor-pointer border-t-2 border-r-2 border-b-2 border-gray-400 h-12 pr-2 rounded-r-lg hover:bg-gray-100"
                >
                  <i className="fas fa-search pl-4 pr-2 text-gray-500" />
                </button>
              </form>
            </motion.div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
