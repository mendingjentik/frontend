import { motion } from "framer-motion";
import { Fragment } from "react/cjs/react.production.min";

export default function Popup(props) {
  const { title } = props;
  return (
    <motion.div
      className="fixed mt-12 w-96 ml-24 rounded-xl shadow-lg p-8 pt-0 overflow-y-scroll"
      initial={{ x: 100, opacity: 0 }}
      animate={{ x: 0, opacity: 1, transition: { delay: 0.1 } }}
      exit={{ x: 100, opacity: 0 }}
      style={{ maxHeight: "75vh" }}
    >
      <h3 className="font-semibold text-3xl mb-4 sticky top-0 bg-white py-4 border-b-2">
        {title}
      </h3>
      <div>
        {Object.keys(props).map((field) => {
          if (["title", "actor"].includes(field)) return;
          if (props[field] == "nan") return;
          return (
            <div className="mb-2">
              <span className="font-semibold">{field}</span>:{" "}
              {props[field].includes("dbpedia.org") ? (
                <a
                  href={props[field]}
                  className="text-blue-500"
                  target="_blank"
                >
                  {props[field]
                    .slice(props[field].includes("https") ? 29 : 28)
                    .replace("_", " ")}
                </a>
              ) : (
                props[field]
              )}
            </div>
          );
        })}
        <div>
          {!["nan", ""].includes(props.actor) && (
            <Fragment>
              <span className="font-semibold">actors</span>:
              <ul className="ml-8">
                {props.actor.split(", ").map((actor) => {
                  if (actor == "") return;
                  if (actor == "https://dbpedia.org/resource/") return;
                  if (actor == "http://dbpedia.org/resource/") return;
                  if (actor.slice(29) == "")
                    return <li className="list-disc">{actor}</li>;
                  return (
                    <li className="list-disc">
                      <a className="text-blue-500" href={actor} target="_blank">
                        {actor
                          .slice(actor.includes("https") ? 29 : 28)
                          .replace(/_/g, " ")}
                      </a>
                    </li>
                  );
                })}
              </ul>
            </Fragment>
          )}
        </div>
      </div>
    </motion.div>
  );
}
